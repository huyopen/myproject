import * as types from './account.types';

const initState = {
    username : '',
    accessToken: '',
    refreshToken: '',
    isLoading: false
}

export default function accountReducer(state = initState, action){
    switch(action.type){
        case types.LOGIN: {
            if(action.payload.username === "huyopen" && action.payload.password === "123456"){
                return {
                    ...state,
                    username : action.payload.username,
                    accessToken: "1111",
                    refreshToken: "2222"
                }
            }
        }
        case types.LOGOUT: {
            return{
                ...state,
                username: '',
                accessToken: '',
                refreshToken: ''
            }
        }
        default: {
            return state
        }
    }
}