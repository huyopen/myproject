import * as types from './account.types';

export const loginAction = (username,password) => {
    return {
        type: types.LOGIN,
        payload:{
            username,
            password
        }
    }
}
export const registerAction = (username,password) => {
    return {
        type: types.REGISTER,
        payload: {
            username,
            password
        }
    }
}
export const logoutAction = () =>{
    return {
        type: types.LOGOUT  
    }
}