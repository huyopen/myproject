/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{useState,useEffect} from 'react';
import { Provider, useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import Login from './component/account/login/Login';
import Register from './component/account/register/Register';
import Home from './component/home/Home';
import { store } from './configs/store.config';
import auth from '@react-native-firebase/auth';
import Scanner from './component/qrcode/Scanner';



const Stack = createStackNavigator();
const App = () => {
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();
    const onAuthStateChanged = (user) => {
        setUser(user);
        if (initializing) setInitializing(false);
    }
    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber; // unsubscribe on unmount
    }, []);
    const accountState = useSelector(state => state.accountsReducer);
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
            }}>
                {!user ? (
                    <>
                        <Stack.Screen name="Login" component={Login} options={{
                            headerShown: false
                        }} />
                        <Stack.Screen name="Register" component={Register} options={{
                            headerShown: false
                        }} />
                    </>
                )
                    :
                    (
                        <>
                            <Stack.Screen name="Home" component={Home} options={{
                                headerShown: false
                            }} />
                            <Stack.Screen name="Scanner" component={Scanner} options={{
                                headerShown: false
                            }} />
                        </>
                    )
                }

            </Stack.Navigator>
        </NavigationContainer>
    );
};
const WrapApp = () => {
    console.log(store.getState());
    return (
        <Provider store={store}>
            <App />
        </Provider>
    )
}





export default WrapApp;
