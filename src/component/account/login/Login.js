/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  SafeAreaView,
  StatusBar,
  Image,
  View,
  StyleSheet,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Text
} from 'react-native';
import { actions } from './../../../redux/account/index';
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch } from 'react-redux';
import { store } from '../../../configs/store.config';
import auth from '@react-native-firebase/auth';


const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');
const Login = ({ navigation }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();
  const changeUsername = (text) => {
    setUsername(text);
  };
  const changePassword = (text) => {
    setPassword(text);
  };
  const onLogin = async() => {
    if (!username || !password) {
      console.log("abc");
    }
    else {
      try {
        await auth()
          .signInWithEmailAndPassword(username, password)
      }
      catch (error) {
        console.log(error)
      }
    }
  };
  const onRegister = () => {
    navigation.navigate("Register");
  }
  return (
    <LinearGradient colors={['#c94b4b', '#4b134f']} style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" />
      <SafeAreaView style={styles.container}>
        <View style={styles.loginContainer} >
          <Image source={require('./../../../ui/assert/images/wiki.png')} />
          <View style={styles.textInputContainer}>
            <TextInput onChangeText={changeUsername} placeholder="Your Email" placeholderTextColor="rgba(255,255,255,0.7)" style={styles.textInput} />
            <TextInput onChangeText={changePassword} placeholder="Your Password" placeholderTextColor="rgba(255,255,255,0.7)" secureTextEntry={true} style={[styles.textInput, { marginTop: 20 }]} />
          </View>
          <TouchableOpacity onPress={onLogin} style={styles.buttonContainer}>
            <Text style={styles.textButton}>LOGIN</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={onRegister} style={styles.registerButton}>
            <Text style={styles.registerText}>Don't have any account? Register here</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  loginContainer: {
    flex: 1, alignItems: 'center', justifyContent: 'center',flexDirection: 'column'
  },
  textInputContainer: {
    margin: 20, width: WIDTH, alignItems: 'center'
  },
  textInput: {
    borderWidth: 1, width: '80%', borderRadius: 35, borderColor: '#fff', paddingHorizontal: 20, fontSize: 17, color: '#fff', height: 70
  },
  buttonContainer: {
    height: 80, width: '80%', alignItems: 'center', justifyContent: 'center', backgroundColor: '#2ebf91', borderRadius: 50
  },
  textButton: {
    color: '#fff', fontSize: 17, fontWeight: 'bold'
  },
  registerButton: {
    marginTop: 10,
    alignItems: 'center',
  },
  registerText: {
    color: 'rgba(255,255,255,0.7)',
    fontSize: 16,
  }
});


export default Login;