/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StatusBar,
  View,
  StyleSheet,
  Dimensions,
  Text
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { TouchableOpacity } from 'react-native-gesture-handler';
import auth from '@react-native-firebase/auth';


const { width: WIDTH, height: HEIGHT } = Dimensions.get('window');
const Home = ({ navigation }) => {
  const signout = () => {
    auth()
      .signOut()
      .then(() => console.log('User signed out!'));
  }
  const camera = () => {
    navigation.navigate("Scanner");
  }
  return (
    <LinearGradient colors={['#c94b4b', '#4b134f']} style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" />
      <SafeAreaView style={styles.container}>
        <View style={styles.loginContainer} >
          <TouchableOpacity onPress={signout}>
            <View style={styles.buttonContainer}>
              <Text style={styles.textButton}>Logout</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={camera}>
            <View style={styles.buttonContainer}>
              <Text style={styles.textButton}>Scanner</Text>
            </View>

          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  loginContainer: {
    flex: 1, alignItems: 'center', justifyContent: 'center'
  },
  textInputContainer: {
    margin: 20, width: WIDTH, alignItems: 'center'
  },
  textInput: {
    borderWidth: 1, width: '80%', borderRadius: 35, borderColor: '#fff', paddingHorizontal: 20, fontSize: 17, color: '#fff', height: 70
  },
  buttonContainer: {
    width: 200, height: 50, margin: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: '#99f2c8', borderRadius: 20
  },
  textButton: {
    color: 'rgba(0,0,0,0.7)', fontWeight: 'bold' 
  },
  registerButton: {
    marginTop: 10,
    alignItems: 'center',
  },
  registerText: {
    color: 'rgba(255,255,255,0.7)',
    fontSize: 16,
  }
});


export default Home;