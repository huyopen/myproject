import { combineReducers } from 'redux'
import {
    accountsReducer
} from './../redux/index';
const rootReducer = combineReducers({
  accountsReducer,
})
export default rootReducer
