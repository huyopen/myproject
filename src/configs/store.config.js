import { createStore } from 'redux'
import rootReducer from './reducer.config';

const store = createStore(rootReducer)
export { store }
